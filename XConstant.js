var XConstant = {};
(function () {
    this.ContentTypeId = {};
    this.List = {};
    this.Url = {};
    this.SubSite = {};
}).apply(XConstant);

(function () {
    this.SYSTEM = "0x";
    this.ITEM = "0x01";
    this.DOCUMENT = "0x0101";
    this.FOLDER = "0x0120";
}).apply(XConstant.ContentTypeId);

(function () {
}).apply(XConstant.SubSite);

(function () {
    // try _spPageContextInfo.serverRequestPath or _spPageContextInfo.webServerRelativeUrl in your console for your reference
    this.SITES = "";
    this.DEFAULT_PROFILE_IMAGE = "/SiteAssets/html/img/profile-img.png";
    this.RAStructure = this.SITES + "/Pages/ReportingAuthorityStructure.aspx";
}).apply(XConstant.Url);

(function () {
}).apply(XConstant.Url.SubSite);

(function () {
}).apply(XConstant.List);
